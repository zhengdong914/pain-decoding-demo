% draw z-score plots based on different options
% one figure for each trial
% author: Zhengdong Xiao
% date: 2017-3-13
function drawZscorePlots_Correlation(z_scores,seq,options)

    decoders = options.decoders;
    if strcmp(options.plotOpt.region,'all')
        region = seq.region;
    else
        region = options.plotOpt.region;
    end

    legends = {};
    edges = [-options.TPre:options.binsize:options.TPost];

    if strcmp(options.plotOpt.trials,'all')
        trials = (1:length(eval(['z_scores.' decoders{1} '.' options.region{1}]))-1);
    else
        trials = options.plotOpt.trials;
    end
    st = 1;
    et = length(trials);
    single_trial = options.trial;
    if (single_trial>=1 && single_trial<=et)
        trials = single_trial;
        et = 1;
    end
  
    for t=st:et
        figure('name',[' trial' num2str(trials(t))]);
        subplotIdx = '411';
        seqall = eval(['seq.' seq.region{1} '(trials(t))']);
        titleStr = ['Trial' num2str(trials(t)) ' ' region{1} '=1:' num2str(size(seqall.y,1))];        
        if(length(seq.region)==1)
            seqall.y = [seqall.y;eval(['seq.' seq.region{1} '(trials(t)).y'])];
        else
            seqall.y = [seqall.y;eval(['seq.' seq.region{2}  '(trials(t)).y'])];
        end
        
        drawRasterTrialNew(seqall,titleStr,edges,subplotIdx);          
        
        % lower plot
        subplotIdx = '412';
        % draw baseline
        legends = drawBaseline(options,edges,legends,subplotIdx);
        legends = drawThresh(options,edges,legends,subplotIdx);

        for r=1:length(region)
            if (r==2)                                
                if (length(seq.region)==1)
                    z_score = eval(['z_scores.' decoders{1} '.' seq.region{1} '(trials(t));']);
                else
                    z_score = eval(['z_scores.' decoders{1} '.' seq.region{2} '(trials(t));']);
                end
                l_z = length(z_score.zscore) - 1;
                decoder_edges = [-options.TPre:options.binsize:l_z*options.binsize - options.TPre];
                if options.currentTrial
                    decoder_edges = decoder_edges + options.binsize;
                end
                color = options.plotOpt.zscoreColor(d+r-1);
                legends = drawZscoreTrial(z_score,decoder_edges,legends,decoders{d},region{r},options,color,subplotIdx);                           
            else
                for d=1:length(decoders)
                    z_score = eval(['z_scores.' decoders{1} '.' seq.region{1} '(trials(t));']);
                    l_z = length(z_score.zscore) - 1;
                    decoder_edges = [-options.TPre:options.binsize:l_z*options.binsize - options.TPre];
                    if options.currentTrial
                        decoder_edges = decoder_edges + options.binsize;
                    end
                    color = options.plotOpt.zscoreColor(d+r-1);
                    legends = drawZscoreTrial(z_score,decoder_edges,legends,decoders{d},region{r},options,color,subplotIdx);
                end
            end
        end

        % draw event
        if isfield(seq,'withdrawal')
            withdrawEvent = seq.withdrawal;
        else
            withdrawEvent = [];
        end
        legends = drawEvent(withdrawEvent,options,legends,trials(t),subplotIdx);

        ylabel('Z-score','fontsize',24);
        set(gca,'fontsize',20);
        ylim(options.plotOpt.ylim);
%         xlabel('time(s)','fontsize',24);
        xlim([-options.TPre,options.TPost])
        subplot(subplotIdx)
%         lgd = legend(legends);
%         lgd.FontSize = 10;
        legends = [];
%         title(options.plotOpt.plotTitle);

        subplotIdx = '413';         
        drawBaseline(options,edges,legends,subplotIdx);             
           
        for rou=1:length(options.rou)-1
            cct = eval(['z_scores.' decoders{1} '.' seq.region{1} '(trials(t)).ccvalue(:,:,rou);']);                
            options.C_thresh=eval(['z_scores.' decoders{1} '.' seq.region{1} '(trials(t)).ccthresh(:,:,rou);']);
            inx1 = find(cct(options.TPre/options.binsize:(options.TPre+2)/options.binsize)>options.C_thresh(2) | cct(options.TPre/options.binsize:(options.TPre+2)/options.binsize)<options.C_thresh(1));
            inx2 = find(cct((options.TPre+2)/options.binsize:(options.TPre+options.TPost)/options.binsize)>options.C_thresh(1) & cct((options.TPre+2)/options.binsize:(options.TPre+options.TPost)/options.binsize)<options.C_thresh(2));
            if ~isempty(inx1)&&~isempty(inx2)
                options.crossvalue=[(inx1(1)-1)*options.binsize (inx2(1)-1)*options.binsize+2];
            else
                options.crossvalue=[];
            end
            color = options.plotOpt.zscoreColor(4-rou);
            legends = drawCorrelationPlots(cct,options,edges,legends,subplotIdx,color,options.rou(rou));
            if(options.rou(rou)==0.6)
                area=zeros(size(cct));
                for tt=2:length(cct)
                    if(cct(tt)>options.C_thresh(2))
                        area(tt)=area(tt-1)+cct(tt)-options.C_thresh(2);
                    elseif(cct(tt)<options.C_thresh(1))
                        area(tt)=area(tt-1)+options.C_thresh(1)-cct(tt);
                    end
                end
            end
        end
               
        ylabel('CCF','fontsize',24);
        set(gca,'fontsize',20);
        xlim([-options.TPre,options.TPost])
        ylim([-2,2]);
%         xlabel('time (s)','fontsize',24);
        subplot(subplotIdx)
        lgd = legend(legends);
        lgd.FontSize = 10;
        legends = [];
        if ~isempty(options.crossvalue)
%             title(['Cross Thresh  On:' num2str(options.crossvalue(1)) 's   Off:' num2str(options.crossvalue(2)) 's     Confidential Interval:' num2str(options.confidenceInterval)]);
        end
        
        subplotIdx = '414';
        subplot(subplotIdx);
        hold on;
        p_area=plot(edges,area,[color '-'],'linewidth',2,'DisplayName',['area above threshold']);
        legends=[p_area];
        p_thr1 = plot([edges(1) edges(end)],[1.6 1.6],[options.plotOpt.threshColor '--'],'linewidth',1,'DisplayName',['Area threshold:' num2str(1)]);
        ylabel(['Area above';'threshold '],'FontSize',24);
        set(gca,'fontsize',20);
        xlim([-options.TPre,options.TPost])
        xlabel('Time (s)','fontsize',24);

    end
end